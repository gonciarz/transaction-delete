package com.example.trans;

import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface TenantService {

    void add(Tenant tenant);

    List<Tenant> getAll();

    void deleteByName(String name);

}
