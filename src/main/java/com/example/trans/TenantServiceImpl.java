package com.example.trans;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.List;

@Service
public class TenantServiceImpl implements TenantService {

    private TenantRepository tenantRepository;

    private AnotherService anotherService;

    public TenantServiceImpl(TenantRepository tenantRepository, AnotherService anotherService) {
        this.tenantRepository = tenantRepository;
        this.anotherService = anotherService;
    }

    @Override
    public void add(Tenant tenant) {
        tenantRepository.save(tenant);
    }

    @Override
    public List<Tenant> getAll() {
        return tenantRepository.findAll();
    }


    @Override
    public void deleteByName(String name) {
        tenantRepository.deleteByName(name);
        boolean result = anotherService.deleteIndex(name);
        if (!result) {
//            throw new RuntimeException();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//
        }

    }

}
