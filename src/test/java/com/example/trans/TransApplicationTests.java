package com.example.trans;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

//@DataJpaTest
@EnableJpaRepositories
@RunWith(SpringRunner.class)
@SpringBootTest
public class TransApplicationTests {

    private static final Tenant TENANT = new Tenant(UUID.randomUUID().toString(), "John");

    @Autowired
    private TenantService tenantService;

    @Autowired
    private EntityManager entityManager;

    @MockBean
    private AnotherService anotherService;

    @Test
    public void shouldDeleteIfBothServicesSucceed() {
        when(anotherService.deleteIndex(anyString())).thenReturn(true);

        tenantService.add(TENANT);
        List<Tenant> resultAfterAdd = tenantService.getAll();
        assertThat(resultAfterAdd)
                .isNotEmpty()
                .containsExactly(TENANT);

        tenantService.deleteByName("John");

        List<Tenant> resultAfterDelete = tenantService.getAll();
        assertThat(resultAfterDelete)
                .isEmpty();
    }

    @Test
    public void shouldRollBackIfSecondServiceFails() {
        when(anotherService.deleteIndex(anyString())).thenReturn(false);

        tenantService.add(TENANT);
        List<Tenant> resultAfterAdd = tenantService.getAll();
        assertThat(resultAfterAdd)
                .isNotEmpty()
                .containsExactly(TENANT);

        tenantService.deleteByName("John");

        List<Tenant> resultAfterDelete = tenantService.getAll();
        assertThat(resultAfterDelete)
                .isNotEmpty()
                .containsExactly(TENANT);
    }

    @Test
    public void shouldRollBackIfExceptionFromSecondService() {
        when(anotherService.deleteIndex(anyString())).thenThrow(new RuntimeException());

        assertThatThrownBy(() -> tenantService.deleteByName("John"))
                .isInstanceOf(RuntimeException.class);

        List<Tenant> resultAfterDelete = tenantService.getAll();
        assertThat(resultAfterDelete)
                .isNotEmpty()
                .containsExactly(TENANT);
    }



}
